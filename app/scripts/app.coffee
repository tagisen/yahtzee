define [
  'underscore'
  'models/game'
  'models/Die'
  'collections/Dice'
  'collections/GameList'
  'views/AppView'
  'views/DiceView'
], ( _, Game, Die, DiceCollection, GameList , AppView, DiceView ) ->


  # The first thing we neeed to check is the localStorage. If there are objects already created, stored the game might
  # have been cut in middle. So we re-create the same game and ask the user if she wants to resume with the game.
  # The user can resume or start over a new game.
  games = new GameList()
  games.fetch()
  if games.length isnt 0
    game = games.at(0)
  else
    game = new Game()


  # Create a dice collection where we will keep all our dices, so we can access their values and events.
  dice = new DiceCollection

  new DiceView
    collection: dice

  dice.fetch()
  # If the localStorage is virgin.
  if dice.length is 0
    for i in [0..4]
      die = new Die()

      dice.add die
      die.save()

  # We listen to newDiceHand, which is an event fired every time a new hand has been rolled
  dice.on 'newDiceHand', (e)->
    game.set 'hand', e.hand
    game.trigger 'analyseHand'
  ,@

  # Create views for the game
  appView = new AppView
    model : game
    collection : dice

  # games.listenTo games, 'add', games.addOne
  games.add(game)
  game.save()

  window.App =
    version : '0.6'
    game : game
    view : appView
    gameList : games
    dice : dice

  return App
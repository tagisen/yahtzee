define [
  'underscore'
  'backbone'
  'models/Die'
  'backboneLocalStorage'
], (_, Backbone, Die, Store) ->

  class DiceCollection extends Backbone.Collection
    initialize: ()->
      @on 'rollDice', @rollDie, @
      @on 'resetDie', @resetDie ,@
    localStorage: new Store("yahtzee-dice")
    model: Die
    rollDie: ()->
      for model in @models
        model.trigger 'roll'
      # After each roll extract all the values.
      @parseHand()

    resetDie: ()->
      for model in @models
        model.trigger 'reset'

    parseHand:()->
      ###
      Code snipped to group numbers by value. Returns an 2x2 array
      with values and number of times its found in the array useful to analyze hand.

      The following code will create a 'hand' array, which is 2x2 array with the unique rolled values
      and the number of times it is present in the rolled hand.

      This event will pas the hand values to the App and this collection had done its job.
      ###
      hand =  _.chain(@pluck('currentValue')).countBy().pairs().value()
      @trigger 'newDiceHand', {hand:hand}

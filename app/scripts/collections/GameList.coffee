define [
  'underscore'
  'backbone'
  'models/Game'
  'backboneLocalStorage'
], (_, Backbone, Game, Store) ->

  class GameList extends Backbone.Collection
    # initialize: ()->
    #   # @on 'rollDice', @rollDie, @
    #   # @on 'resetDie', @resetDie ,@
    #   console.log "Games collection"
    localStorage: new Store("yahtzee-game")
    model: Game
    # addOne:(e)->
    #   console.log "added a game", e

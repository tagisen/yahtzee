#/*global require*/
'use strict'

require.config
  shim:
    zepto :
      exports : 'zepto'
    underscore:
      exports: '_'
    backbone:
      deps: [
        'underscore'
      ]
      exports: 'Backbone'
    handlebars:
      exports: 'Handlebars'
    moment:
      exports: 'Moment'
    fastclick :
      exports : 'fastclick'
    backboneLocalStorage :
      deps: ['backbone']
      exports: 'Store'

  paths:
    jquery : '../bower_components/jquery/dist/jquery.min'
    backbone: '../bower_components/backbone/backbone'
    underscore: '../bower_components/underscore/underscore'
    handlebars: '../bower_components/handlebars/handlebars'
    moment: '../bower_components/moment/moment'
    fastclick : '../bower_components/fastclick/lib/fastclick'
    backboneLocalStorage: '../bower_components/backbone.localstorage/backbone.localStorage'

require [
  'jquery','backbone', 'moment', 'fastclick', 'app' , 'routes/router', 'collections/Dice' , 'vendor/LocalScoreManager' , 'vendor/HandlebarsHelpers'
], ($, Backbone, moment, FastClick, App, Router, DiceCollection, LocalScoreManager) ->

  App.router = new Router()
  FastClick.attach(document.body)

  Backbone.history.start()


  return
define [
  'underscore'
  'backbone'
  'views/DieView'
], (_, Backbone, DieView) ->
  'use strict';

  class DieModel extends Backbone.Model
    currentValue: 1
    handLimit : 3  # Number of times the player can roll the dice per turn. Useful for tesing, we can set it to bigger val.
    selected: false
    url : '#/die'
    values:
      1: 270:270
      2: 0:0
      3: 0:270
      4: 0:180
      5: 0:90
      6: 90:270
    initialize: ()->
      self = @

      @on 'roll', @assignValue , @
      @on 'hold', @hold, @
      @on 'release', @release, @
      @on 'reset', @resetDie, @
      # if @get('hand') is undefined
      @resetDie()

      @save()

    assignValue: ()->
      self = @
      if @get('hand') < @handLimit
        # No matter if we assign new value or not, we increase the hand.
        @set('hand', (@get('hand') + 1) )

        ###
        Assing new value to the die only if its not put on hold
        from the player. This will prevent the animation as well, since if there
        is no change in this.currentValue, which we are listening to from the DieView.
        ###
        if not @get('onHold')
          ###
          We assign 0 to current value, just to force this.currentValue change.
          If we remove the next line, and in case of the new value is the same
          as the old one, this.currentValue:change will not tigger.
          ###
          rnd = Math.round Math.random() * (6 - 1) + 1
          @set 'currentValue', 0
          @set 'currentValue' , rnd

        @save()
    resetFace:()->
      @set 'currentValue' , 6

    toggleSelected:()->
      @set 'onHold' , !@.get 'selected'
      @save

    hold:()->
      if @get('hand') isnt 0
        @set 'onHold', true
        @trigger 'holdDieView'
        @save()

    release:()->
      @set 'onHold', false
      @trigger 'releaseDieView'
      @save()

    gameStarted:()->
      @set 'playing', true

    gameFinished:()->
      @set 'playing', false
      @trigger 'releaseDieView'

    resetDie:()->
      @set('hand', 0)
      @release()
      @resetFace()

define [
  'underscore'
  'backbone'
  'moment'
], (_, Backbone, moment) ->
  'use strict';

  class GameModel extends Backbone.Model
    hand      : [0]
    url : '#/game'
    displayHandNames :
      '1' : 'Ones'
      '2' : 'Twos'
      '3' : 'Threes'
      '4' : 'Fours'
      '5' : 'Fives'
      '6' : 'Sixes'
      'bonus' : 'Bonus'
      'twoPair' : 'Two Pairs'
      'threeOfK' : 'Three Of a Kind'
      'fourOfK' : 'Four of a Kind'
      'fullHouse' : 'Full House'
      'sStraight' : 'Small Straight'
      'lStraight' : 'Large Straight'
      'chance'  : 'Chance'
      'yahtzee' : 'Yahtzee'
    handNames : ['1', '2', '3', '4', '5', '6', 'bonus',
                 'twoPair', 'threeOfK','fourOfK', 'fullHouse'
                 'sStraight', 'lStraight' ,'chance' ,'yahtzee']

    registered : []
    score : 0
    initialize: ()->
      self = @


      # @assignValue()

      @on 'analyseHand', @analyse , @
      @set 'handNames', @handNames
      @on 'startNewGame', @reset, @
      @on 'resetHandValues', @calculateTotal, @
      @on 'resetHandValues', @resetHands, @
      @on 'resumeGame', @continue, @
      @on 'gameOver', @end, @

      @save()

    start:()->
      @set 'started', moment().format()
      @set 'running', true
      @set 'status', 'playing'
      @save()

    end:()->
      startMoment = @get 'started'
      duration = moment( moment().format() ).diff(startMoment, 'seconds')
      @set 'duration', duration
      @set 'running', false
      @set 'status', 'end'
      @save()

    reset:()->

      self = @
      _(@handNames).each (item)->
        self.set item, 0
      @set 'registered', []
      @set 'score', 0
      @on 'resetHandValues', @resetHands, @
      @start()

    continue:()->
      @set 'running', true
      console.log "#####Continueing Continue new game"



    resetHands: ()->
      self = @
      hand = @handNames
      _(hand).each (yahtzeeHand)->
        self.set yahtzeeHand, 0 if yahtzeeHand not in self.get 'registered'

      @trigger 'handReset'


    ###
    Analyse hand. This function will be fired after every
    dice roll complete.
    TODO: extract the rolled hands for the turn.
    ###
    calculateTotal:()->

      self = @
      handNames = self.get('registered')
      total = 0
      self.set('total', total)
      _(handNames).each (hand)->
        total +=  self.get(hand)
        self.set('total', total)


    checkBonus:()->

      self = @
      if 'bonus' in self.get 'registered'
        # Bonus is already registered hand
        return
      basic = ['1','2','3','4','5','6']
      if _.intersection(basic , @get('registered')).length is basic.length
        bonus = 0
        _(basic).each (item)->
          bonus += self.get(item)
        if bonus >= 63
          self.set 'bonus', 50
          self.get('registered').push('bonus')

        else
          self.set 'bonus', 0
          self.get('registered').push('bonus')
        self.updateScore('bonus')
        self.trigger('notifyBonusPoints')

    registerHand: (key)->
      @get('registered').push(key)
      @updateScore(key)
      if @get('registered').length is @handNames.length
        @trigger 'gameOver'
      @save()

    updateScore: (key)->
      @set('score', (@get('score') + @get(key)))
      @save()

    analyse:()->
      self = @
      hand = @get 'hand'
      # the length of the hand tells a lot for the rolled hand.
      ln = hand.length
      dieValues = _(@get('hand')).pluck(0).map (item)->
       return parseInt(item)
      nrTimes = _(@get('hand')).pluck(1)


      @resetHands()
      ###
      First of all we set all the values that are rolled for each base numbers (i.e ones, twos .... sixes)
      ###
      chance = 0
      _(hand).each (die)->
        self.set die[0], parseInt(die[0]) * die[1] if die[0] not in self.get 'registered'
        chance += die[0]*die[1]
      self.set 'chance', chance if 'chance' not in self.get 'registered'

      # then we analyse the hand for extra (special) hands.
      switch ln
        when 5

          if _(dieValues).min() is 1 and _(dieValues).max() is 5
            self.set 'lStraight', 40 if 'lStraight' not in self.get 'registered'
            self.set 'sStraight', 30 if 'sStraight' not in self.get 'registered'

          if _(dieValues).min() is 2 and _(dieValues).max() is 6
            self.set 'lStraight', 40 if 'lStraight' not in self.get 'registered'
            self.set 'sStraight', 30 if 'sStraight' not in self.get 'registered'

          if _(dieValues).intersection([1,3,4,5,6]).length is 5
            self.set 'sStraight', 30 if 'sStraight' not in self.get 'registered'

        when 4
          if _(dieValues).min() is 1 and _(dieValues).max() is 4
            self.set 'sStraight', 30 if 'sStraight' not in self.get 'registered'
          if _(dieValues).min() is 2 and _(dieValues).max() is 5
            self.set 'sStraight', 30 if 'sStraight' not in self.get 'registered'
          if _(dieValues).min() is 3 and _(dieValues).max() is 6
            self.set 'sStraight', 30 if 'sStraight' not in self.get 'registered'
        when 3
          twoPairCount = 0
          twoPairSum = 0
          for i in [0...ln]
            if hand[i][1] is 3
              self.set 'threeOfK', parseInt(hand[i][0]) * 3 if 'threeOfK' not in self.get 'registered'
            if hand[i][1] is 2
              twoPairCount += 1
              twoPairSum += parseInt(hand[i][0]) * 2
          if twoPairCount is 2
            self.set 'twoPair', twoPairSum if 'twoPair' not in self.get 'registered'

        when 2
          # We sort the array with the information on how many times the numbers are present in the hand
          ###
          # now that we have only two different numbers in the roll, it means that we could have a full house, or
          # four of a kind
          ###
          # We sort the number of times array, so we know what elements are where
          sorted = _(nrTimes).sort()
          ###
          # if the forst element of the sorted array is 2, then it means that we have a full house, becuase the second
          # element must be 3
          ###
          if sorted[0] is 2 # here its obvious that sorted[1] is 3
            fullHouseSum = ( parseInt(hand[0][0]) * hand[0][1] ) + ( parseInt(hand[1][0]) * hand[1][1] )
            self.set 'fullHouse', fullHouseSum if 'fullHouse' not in self.get 'registered'
            twoPairSum = ( parseInt(hand[0][0]) * 2 ) + ( parseInt(hand[1][0]) * 2 )
            self.set 'twoPair', twoPairSum
          # Othwer
          if sorted[0] is 1
            # Its not the best way to do the following, but doesnt really matter. There are max 2 iterations here.
            for i in [0...ln]
              if hand[i][1] is 4
                self.set 'fourOfK', parseInt(hand[i][0]) * hand[i][1] if 'fourOfK' not in self.get 'registered'
                self.set 'threeOfK', parseInt(hand[i][0]) * 3 if 'threeOfK' not in self.get 'registered'
                self.set 'twoPair', parseInt(hand[i][0]) * 4
          for i in [0...ln]
            if hand[i][1] is 3
              self.set 'threeOfK', parseInt(hand[i][0]) * 3 if 'threeOfK' not in self.get 'registered'
          # if parseInt(hand[0][0]) is not parseInt(hand[1][0])
          #   self.set 'twoPair', (parseInt(hand[0][0]) * 2 + parseInt(hand[1][0]) * 2 ) if 'twoPair' not in self.get 'registered'
        when 1
          self.set 'yahtzee', 60 if 'yahtzee' not in self.get 'registered'
          self.set 'threeOfK', parseInt(hand[0]) * 3 if 'threeOfK' not in self.get 'registered'
          self.set 'fourOfK', parseInt(hand[0]) * 4 if 'fourOfK' not in self.get 'registered'

      self.checkBonus()

      @trigger 'newHands'
define [
  'underscore'
  'backbone'
  'app'
], ( _, Backbone, App) ->
  return Backbone.Router.extend
    routes:
      ''   : 'startScreen'
      'game' : 'play'
      'game/over' : 'gameOverScreen'
      'game/pause' : 'gameOverScreen'
    initialize: ()->
      self = @
      # for view of App.view.subviews
      #   App.view.subviews[view].hide()
      App.game.on 'gameOver', ()->
        self.navigate 'game/over', true


    play : ()->
      # # setTimeout needed to delay the event firing from the game model, so we wait until the game has been properly loaded
      # setTimeout ()->
      #   # App.game.start()
      #   App.game.trigger 'startNewGame'
      # ,20
      gameRunning =  App.game.get('running')
      if gameRunning
        App.game.trigger 'resumeGame'
      else
        App.game.trigger 'startNewGame'

      for view of App.view.subviews
        App.view.subviews[view].hide()
      App.view.subviews.game.show()

    startScreen : ()->
      for view of App.view.subviews
        App.view.subviews[view].hide()
      App.view.subviews.startscreen.show()
    gameOverScreen : ()->
      for view of App.view.subviews
        App.view.subviews[view].hide()
      App.view.subviews.endscreen.show()
      App.view.subviews.game.show()

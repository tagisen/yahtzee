define [
  'underscore'
  'handlebars'
], ( _, Handlebars) ->
  Handlebars.registerHelper 'fullName', (obj) ->
    return "Isen Beqiri";

  Handlebars.registerHelper 'isRegistered', (field) ->
    if field in game.get('registered')
      return "col-xs-3 registered";
    return "col-xs-3";
  Handlebars.registerHelper 'handPicInfo', (item)->
    keys = Object.keys(item)
    # This is to handle game information
    if keys[0] is "Hint"
      hint = item[keys[0]]
      return new Handlebars.SafeString '<div class="col-sm-8">' + hint + '</div>'

    if !keys or keys.length is 0
      return new Handlebars.SafeString '<div class="col-sm-8"> No hands to register. You have to register 0 points.</div>'

    ret = '<div class="hand-pick-col"  > <p data-reg="' + keys[0] + '">'
    for key in keys
      ret += game.displayHandNames[key] + ':' + item[key]
    ret += '</p> </div>'
    return new Handlebars.SafeString ret

  Handlebars.registerHelper 'keyValue', (obj)->
    console.log obj, typeof(obj)
    key = Object.keys(obj)[0]
    val = obj[key]
    ret = '<span class="col-xs-3"  >' + key + ' "</span> <span class="col-xs-3">' + val + '</span>'
    return new Handlebars.SafeString ret



LocalScoreManager = ->
  @key = "bestScore"
  supported = @localStorageSupported()
  @storage = (if supported then window.localStorage else window.fakeStorage)
  return
window.fakeStorage =
  _data: {}
  setItem: (id, val) ->
    @_data[id] = String(val)

  getItem: (id) ->
    (if @_data.hasOwnProperty(id) then @_data[id] else `undefined`)

  removeItem: (id) ->
    delete @_data[id]

  clear: ->
    @_data = {}

LocalScoreManager::localStorageSupported = ->
  testKey = "test"
  storage = window.localStorage
  try
    storage.setItem testKey, "1"
    storage.removeItem testKey
    return true
  catch error
    return false
  return

LocalScoreManager::get = ->
  @storage.getItem(@key) or 0

LocalScoreManager::set = (score) ->
  @storage.setItem @key, score
  return
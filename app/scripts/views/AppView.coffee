define [
  'underscore'
  'backbone'
  'views/ScoreBoardView'
  'views/HandlersView'
  'views/StartScreenView'
  'views/EndScreenView'
  'views/GameView'
], ( _, Backbone, ScoreBoardView, HandlersView, StartScreenView, EndScreenView, GameView) ->
  class AppView extends Backbone.View
    el : '.container'
    constructor: ()->
     # Define the subviews object off of the prototype chain
     @subviews = {};
     # @model.on 'change:status', @updateStatus, @
     # Call the original constructor
     Backbone.View.apply(this, arguments);
     # @model.on 'change:status', @updateStatus, @

    initialize:()->
      @subviews.scoreboard = new ScoreBoardView
        collection: @collection
        model: @model

      @subviews.handlers = new HandlersView
        collection: @collection
        model: @model

      @subviews.startscreen = new StartScreenView
        model: @model

      @subviews.endscreen = new EndScreenView
        model: @model

      @subviews.game = new GameView
        model: @model

      @model.on 'change', @updateProgress, @

      @updateProgress()
      
    updateProgress: ()->
      if @model.get('registered')
        progress = (@model.get('registered').length / @model.handNames.length) * 100
        @el.querySelector('progress').value = progress
        if progress is 100
          @model.trigger 'gameOver'

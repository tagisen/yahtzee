define [
  'underscore'
  'backbone'
  'templates'
], ( _, Backbone, JST) ->
  class BaseView extends Backbone.View
    constructor: ()->
     # Define the subviews object off of the prototype chain
     @subviews = {};
     # @model.on 'change:status', @updateStatus, @
     # Call the original constructor
     Backbone.View.apply(this, arguments);
     @model.on 'change:status', @updateStatus, @

    initialize:()->
      @model.on 'change', @updateStatus, @
    updateStatus: ()->
      @gameStatus =  @model.get 'status'

    hide : ()->
      @el.classList.add('hide')
      @el.classList.remove('show')

    show : ()->
      @el.classList.remove('hide')
      @el.classList.add('show')
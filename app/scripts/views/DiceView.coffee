define [
  'underscore'
  'backbone'
  'templates'
  'views/BaseView'
  'views/DieView'
], ( _, Backbone, JST, BaseView, DieView) ->
  class DiceView extends Backbone.View
    el : document.querySelector('.dice-container')

    initialize: ()->

      self = @
      # @collection.on 'add', @addOne(), @
      @listenTo(@collection, 'add', @addOne);
      @on 'reset', ()->
        console.log "WTREST"
      ,@

      # @model.on 'startNewGame',@showGameContent,@
      # @model.on 'gameOver', @hideGameContent, @
      # @model.on 'startScreen', @hideGameContent, @
    addOne:(die)->
      view = new DieView
        model:die
      # dice.add die
      view.roll(die.get('currentValue'))
      @el.appendChild(view.el)

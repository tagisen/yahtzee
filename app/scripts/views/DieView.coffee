define [
  'underscore'
  'backbone'
  'templates'
  'handlebars'
  'views/BaseView'
], (_, Backbone, Templates, Handlebars, BaseView) ->
  class DieView extends BaseView
    events:
      'click .dice-holder' : 'changeSelection'

    initialize:()->
      self = this
      self.model.on 'holdDieView', ()->
        self.holdDieView()

      self.model.on 'releaseDieView', ()->
        self.releaseDieView()

      self.model.on 'change:currentValue', () ->
        self.roll self.model.get('currentValue')
      ,@

      self.render()


      if @model.get 'onHold'
        @holdDieView()
      # if @model.get 'currentValue'
      #   self.roll(@model.get 'currentValue')

    roll: (value)=>
      self = @

      self.rollAnimation()
      setTimeout ()->
        self.rollRandom(value)
      , 300

    rollRandom:(value)->
      self = @
      die = self.el.querySelector('.dice')
      values = @model.values
      currentValue = @model.get 'currentValue'
      currentSide = values[currentValue]
      if currentSide
        key = Object.keys(currentSide)[0]
        X = Object.keys(currentSide)[0]
        Y = currentSide[X]
        die.style.webkitTransform = "rotateX("+X+"deg)rotateY("+Y+"deg)"
        die.style.MozTransform = "rotateX("+X+"deg)rotateY("+Y+"deg)"
        die.style.msTransform = "rotateX("+X+"deg)rotateY("+Y+"deg)"
        die.style.transform = "rotateX("+X+"deg)rotateY("+Y+"deg)"
      return

    rollAnimation:()=>
      self = @
      die = self.el.querySelector('.dice')
      dieJ = self.el.querySelector('.dice')
      for i in [0..13]
        if die.style.webkitTransform
          current = die.style.webkitTransform.match(/\(.*?\)/g)
          X = parseInt(current[0].match(/[0-9]/g).join("")) + 15
          Y = parseInt(current[1].match(/[0-9]/g).join("")) + 15
        else
          X = 90
          Y = 90
        die.style.webkitTransform =  "rotateX("+X+"deg)rotateY("+Y+"deg)"
        die.style.MozTransform =  "rotateX("+X+"deg)rotateY("+Y+"deg)"
        die.style.msTransform =  "rotateX("+X+"deg)rotateY("+Y+"deg)"
        die.style.transform =  "rotateX("+X+"deg)rotateY("+Y+"deg)"

    render: ()->
      self = @


      html = @template()

      d = document.createElement 'div'
      d.innerHTML = html

      self.el.appendChild(d)


      return


    template: JST['app/templates/die.hbs']

    changeSelection: (event)->
      self = @
      if @model.get 'onHold'
        @model.trigger('release')
      else
        @model.trigger('hold')

    holdDieView:()->
      @el.querySelector('.dice-holder').classList.add('selected')

    releaseDieView:()->
      @el.querySelector('.dice-holder').classList.remove('selected')

define [
  'underscore'
  'backbone'
  'templates'
  'views/BaseView'
], ( _, Backbone, JST, BaseView) ->
  class EndScreenView extends BaseView
    el : document.querySelector('.end-screen')
    initialize:(r)->
      self = @
      @model.on 'change:score', @updateCurrentScore, @
      @render()

    template: JST['app/templates/endscreen.hbs']
    render: ()->
      self = @
      rawTemplate = @template
      score = @model.get 'score'
      self.el.innerHTML = @template({score:score})
    updateCurrentScore: ()->
      @el.querySelector('.current-score').innerHTML = @model.get 'score'

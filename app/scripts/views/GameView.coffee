define [
  'underscore'
  'backbone'
  'templates'
  'views/BaseView'
], ( _, Backbone, JST, BaseView) ->
  class GameView extends BaseView
    el : document.querySelector('.yahtzee-container')

    template: JST['app/templates/DiceView.hbs']

    initialize: ()->

      self = @
      # @model.on 'startNewGame',@showGameContent,@
      # @model.on 'gameOver', @hideGameContent, @
      # @model.on 'startScreen', @hideGameContent, @

    showGameContent:()->
      @el.classList.add('show');
      @model.reset()
    hideGameContent:()->
      @el.classList.remove('show');
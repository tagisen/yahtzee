define [
  'underscore'
  'backbone'
  'templates'
  'handlebars'
  'views/BaseView'
], (_, Backbone, JST, Handlebars, BaseView) ->
  class HandlersView extends BaseView
    events:
      'click .roll-button ' : 'roll'

    el: '#handlers'
    initialize:()->
      self = this
      collection = self.collection
      @render()

      self.model.on 'gameOver', self.disableRollButton, @
      self.model.on 'startNewGame', self.enableRollButton , @
      self.model.on 'resetHandValues', self.enableRollButton, @
      # if self.collection.models[0].get('hand') is 3
      #   self.disableRollButton()
    render:()->
      togo = @collection.models[0].handLimit - @collection.models[0].get('hand')
      @el.innerHTML = @template({
        togo: togo
      })

    roll:()->
      self = @
      if @model.get 'running'
        collection = @collection
        collection.trigger 'rollDice'
        @model.set 'rolled', true
        currentHand = self.collection.models[0].get('hand')
        @el.querySelector('.turns-to-go').innerHTML = self.collection.models[0].handLimit - currentHand
        if currentHand is 3
          self.disableRollButton()
    disableRollButton: ()->
      @el.querySelector('.roll-button').setAttribute('disabled', true)
    enableRollButton: ()->
      @el.querySelector('.roll-button').removeAttribute('disabled')
      @el.querySelector('.turns-to-go').innerHTML = @collection.models[0].handLimit

    template: JST['app/templates/handlers.hbs']

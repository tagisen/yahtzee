define [
  'zepto'
  'underscore'
  'backbone'
  'templates'
], ($, _, Backbone, JST) ->
  class ModalView extends Backbone.View
    events:
      'click #startNewGame' : 'startNewGame'

    initialize:()->
      self = @
      console.log "Ele",
      @model.on 'gameOver',@render, @
      @render()

    render:()->
      @el.innerHTML  = @template({points:@model.get('total')})
      self = @

      setTimeout ()->
        self.el.modal()
      ,700
    startNewGame:()->
      self = @
      self.model.trigger 'startNewGame'
      self.el.modal('hide')

    template: JST['app/templates/modal.hbs']

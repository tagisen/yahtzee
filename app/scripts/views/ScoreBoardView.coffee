define [
  'underscore'
  'backbone'
  'templates'
  'handlebars'
  'views/BaseView'
], ( _, Backbone, JST, Handlebars, BaseView) ->
  class ScoreBoardView extends BaseView
    events:
      'click [data-reg]' : 'registerPoints'

    el: '#score-board'
    initialize:()->

      self = this
      collection = self.collection
      game = self.model
      # We split the array so we can list the hands in two columns.
      self.baseHands = game.get('handNames').slice(0,6) #all the keys without bonus key.
      self.extraHands = game.get('handNames').slice(7,game.handNames.length)

      self.currentValue = collection.models[0].get 'currentValue'

      @model.on 'startNewGame', @render, @

      @model.on 'newHands', ()->
        setTimeout ()->
          self.notifyNewHands()
        ,50
      , @

      @model.on 'notifyBonusPoints', @parseBonusPoints, @

      collection.on 'resetDie', ()->
        self.notifyNewHands()

      # @model.on 'resetHandValues', self.updateUI, @

      game.on 'startScreen', @hideView, @
      # game.on 'gameOver', @hideView, @

      @render()

    render:()->
      baseHands = []
      extraHands = []
      score = 0
      registered = @model.get('registered')
      if registered is undefined
        registered = []

      for h in @baseHands
        baseHands.push(
          key   : h
          val   : @model.get(h)
        )
        score += @model.get(h) if h in registered
      for h in @extraHands
        extraHands.push(
          key   : h
          val   : @model.get(h)
        )
        score += @model.get(h) if h in registered

      @el.innerHTML = @template(
        model: @model
        baseHands: baseHands
        extraHands: extraHands
        bonus: @model.get('bonus')
        score: score

      )
      @updateUI()

    updateUI:()->
      hands = @model.handNames
      registeredHands = @model.get('registered') || []
      for hand in hands
        ele = @el.querySelector '.hand-available-' + hand
        if hand in registeredHands
          val = @model.get hand
          pointsSlot = ele.parentNode
          pointsSlot.innerHTML = val
          pointsSlot.classList.add 'locked'
        else
          # handValue = parseInt(@model.get(hand))
          # if handValue > 0
          #   ele.parentNode.classList.add('positive')
          #   ele.classList.add('blinking')
          # else
          ele.parentNode.classList.remove('positive')
          ele.classList.remove('blinking')
          ele.innerHTML = 0



    notifyNewHands:()->
      hands = @model.handNames
      registeredHands = @model.get 'registered'
      for h in hands
        handValue = parseInt(@model.get(h))
        query = '.hand-available-' + h

        element = @el.querySelector(query)
        # icon = element.parentNode.querySelector('.icon-double-angle-left')
        if element and h not in registeredHands
          element.innerHTML = handValue
          if handValue > 0
            element.parentNode.classList.add('positive')
            element.classList.add('blinking')
          else
            element.parentNode.classList.remove('positive')
            element.classList.remove('blinking')
            # icon.classList.remove('dragleft')

    registerPoints: (e)->
      target = e.target
      if !target
        return

      if target.classList.contains('hand-points-slot')
        pointsSlot = e.target
        handsAvailable = pointsSlot.querySelector('.hand-available-data')
      else
        handsAvailable = e.target
        pointsSlot = e.target.parentNode

      if @model.get 'rolled'
        key = e.target.getAttribute 'data-reg'
        registeredHands = @model.get 'registered'
        if key not in registeredHands
          points = @model.get key
          @model.registerHand key
          handsAvailable.classList.add 'hide'
          @model.set 'rolled', false
          @model.trigger 'resetHandValues'
          @collection.trigger 'resetDie'

          pointsSlot.innerHTML = points
          pointsSlot.classList.add('locked')

          @el.querySelector('.total-score').innerHTML = @model.get('score')

    parseBonusPoints:()->
      bonusPlaceHolder = @el.querySelector('.hand-available-bonus').parentNode;
      bonusPlaceHolder.classList.add('locked')
      bonusPlaceHolder.innerHTML = @model.get('bonus')
    template: JST['app/templates/player.hbs']

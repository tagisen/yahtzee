define [
  'underscore'
  'backbone'
  'views/BaseView'
], ( _, Backbone, BaseView) ->
  class StartScreenView extends BaseView
    el : document.querySelector('.start-screen')
    # initialize:(r)->
    # #   @model.on 'gameOver', @resetView, @
    # #   @model.on 'startNewGame', @hideView, @
    # #   @model.on 'startScreen', @resetView, @
    # # hideView : ()->
    # #   @el.classList.add('hide')

    # resetView : ()->
    #   @el.classList.remove('hide')
